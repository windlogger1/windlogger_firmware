# Windlogger firmware v0.8.6

## Introduction

This is is the documentation on the windlogger firmware. This branch implement new feature as SD record button for saving daily data, control the SPI DAC for a better resolution in windlogger measures...

Use the mightyCore atmega1284p as a bobuino variant (new pinout) for digital board >=2.1.0.

## PlatformIO configuration

I'm using VSCode editor with the PlatformIO integration.

This repository contain the code and the platformio config file, ready to build and upload.


1.  Download VSCode
2.  Install the platformio extension
3.  Download or clone this repository
4.  Build the code a first time, platformio will download every dependencies : 'Ctrl+alt+B'
5.  The build normally success
6.  In order to use the timer2 as millis function you need to update 2 files :

 * Copy arduino.h and wiring.c from [HERE](https://gitlab.com/gilou_/arduino_core_improvement)
in .platformio/packages/framework-arduinoavr/cores/MightyCore/

7.  Rebuild the project to include timer2 feature.
8.  Upload the code : 'Ctrl+alt+U'

9.  Have fun!
