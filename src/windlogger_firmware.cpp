#include <Arduino.h>
#include "FSM.h"

//#define DEBUG_MAIN

FSM fsm;		// define a FSM

//The setup function is called once at startup of the sketch
void setup()
{
// Add your initialization code here
	Serial.begin(9600);
	Serial.println("Initialization windlogger v0.8.6");
	pinMode(LED_BUILTIN,OUTPUT);			// initialize the LED_BUILDING as output (pin 13)
	digitalWrite(LED_BUILTIN,HIGH);			// led off

#ifdef DEBUG_MAIN
	Serial.print("test CS22 on TCCR2B ");
	if(TCCR2B&(1<<CS22))
		Serial.println("CS22 on");
		else
			Serial.println("CS22 is not set!!");
	Serial.print("test TOIE2 on TIMSK2 ");
	if(TIMSK2&(1<<TOIE2))
		Serial.println("TOIE2 on");
	else
		Serial.println("TOIE2 is not set!!");
#endif

	fsm.init();

	// Initialize timer3
	TCCR3A = 0;
	TCCR3B = 0;
	TCCR3C = 0;
	TIMSK3 = 0;

	digitalWrite(LED_BUILTIN,LOW);			// led off
}

// The loop function is called in an endless loop
void loop()
{
#ifdef DEBUG_MAIN
	//Serial.println("Main function...");
#endif

	////Add your repeated code here
	// hardware management
	// Serial input processing
	while (Serial.available())
	 {
		char ch=Serial.read();		// copy the char in ch
	 switch(ch)
	   {
	   case '\r':					// if carriage return, pull-up the flag.confgRequest flag
#ifdef DEBUG_MAIN
		   Serial.print("CR");
#endif
	     fsm.flag_configRequest = true;
	     fsm.addChar('\0');
	     break;
	   case '\n':					// if it's new line, just do nothing
		  //
	     break;
	   default:
#ifdef DEBUG_MAIN
		   Serial.print(ch);
#endif
	     fsm.addChar(ch);			// else, add char to the configRequest
	   }
	 }

	// Check timing :
#ifdef DEBUG_MAIN
		   Serial.print("\rcheck timing -> ");
#endif
	fsm.timingControl ();	// check if time comes to do a new measure or not


	// execute state, each next state is decide in the current state
	(fsm.*(fsm.nextState))();
}
